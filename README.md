# TUBULE-TIFF-PICKER

The files in this repository contain code to extract information from tiff files containing cortical microtubule dynamics.

The file **loader.py** allows to select a file an browse through the frames using the right and left arrows or a slider. The file 
**mixer.py** is more specific as it is designed to load two files at the same time, a file which uses a tubulin marker which, in the case of being selected then searches for the sister file using MCherry for the tips, if the files are present, then both are loaded and overlaped, the latter images can be thresholded to show only the tips of the evolving Microtubule. The project is aimed to have a tool to extract collision data and as it stands at the moment is suitable to achieve this task. I remains to be decided if the two files merge into a single project and if more development will be carried out to polish several rough edges.

Apart from the difference described below both files allow to carry out the same tasks as decribed below.


## Getting started

The files are written in python 3, and use the library [**tkinter**](https://docs.python.org/3/library/tkinter.html#module-tkinter) which provides an interface to bindings for producing GUI-Desktop applications using **Tk**, The library is shipped with any standard python 3 installation for OS, Unix/Linux and Windows systems.

The scripts have been so far used on Ubuntu 20.04 and higher. Therefore there is no guarantee  that it will work out of the box on Windows or Mac, but in principle it should.

The script requires also [[skimage]](https://scikit-image.org/docs/stable/install.html) and [[pillow]](https://pillow.readthedocs.io/en/stable/installation.html) for the image handling, as well as the standard **numpy** and **csv** modules.

## Usage.

The file can be launched as a strandard python script on a terminal by typing:

 `$ python loader.py`

Which opens the GUI. 

![LoaderGUI](loader.png)

The GUI consists of:
 
 A top bar with three buttons:

1. New/Load TIFF:  Selects the tiff file to load.
2. Load CSV: Loads a former dataset with stored data.
3. Save: Saves the current session.

A central panel with a canvas for the image, a slider to increase the brightness of the image. A table displaying the results with buttons which execute the tasks:

1. Collision Type: Selects the type of events recorded.
2. Reset View: Displays the full image.  
3. Zoom-In: Zooms the image to a region determined by the mouse.
4. Hide/Show All: Hides or shows all the angles recorded so far.
5. Remove Row: Removes a selected row highlighted in the table.
6. Shows the selected rows highlighted in the results table.

A bottom slider to select the frame, this is also binded to the left and right arrows.

On the canvas, the right mouse button allows to define an record an angle in the present frame. The left mouse button allows to define a square region (spanned by the x direction) in the canvas which can be zoomed.

A session loaded is shown below.

![LoadedGUI](Sessionloaded.png)
