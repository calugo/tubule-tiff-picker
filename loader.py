from tkinter import *
from tkinter import ttk
from tkinter import filedialog as fd
from glob import glob
from skimage import io
from PIL import Image,ImageTk
import numpy as np
import csv


root = Tk()
#root.attributes('-zoomed',True)
root.title('Tiff-Picker')
m=0
n=0
global count, zoomst
zoomst = 0
count=0
countx = 0
points=[]
EList=['X','K','Z','U']
#Scale factors
global lj,xi,xf,yi,yf
global LH,lh,DLH

#The transform L is 800 for zooms
LH=800 #Screen Image length
lh=512 #TIFF Image length
lj=LH/lh
DLH=LH
#Scale and slice factors of the original 512x512 image array
xi=0
xf=800
yi=0
yf=800
ax=0
bx=512
ay=0
by=512
###############################
btnframe=Frame(root)
#btnframe.pack()
btnframe.grid(column=0,row=0)

cnvframe=Frame(root)
cnvframe.grid(column=0, row=1,sticky='w')
#cnvframe.pack()

imframe=Frame(root)
#imframe.pack()
imframe.grid(column=0,row=2)

current_value = DoubleVar()
brvalue= DoubleVar()
colvalue= StringVar(root)
colvalue.set("Collision-type")

global allyn
allyn=BooleanVar()
allyn.set(True)


canvas=Canvas(cnvframe,bg='black',height=800,width=800)
canvas.grid(column=0,row=0,sticky='W',rowspan=3)

####
#Treeview 
Wn=70
table_pd = ttk.Treeview(cnvframe)
#table_pd['columns']=("X1","Y1","X2","Y2","X3","Y3","Angle","Event","Frame","Zoom")
table_pd['columns']=("X1","Y1","X2","Y2","X3","Y3","Angle","Event","Frame")
table_pd.column("#0",width=0,stretch=NO)
table_pd.column("X1",anchor=W,width=Wn)
table_pd.column("Y1",anchor=W,width=Wn)
table_pd.column("X2",anchor=W,width=Wn)
table_pd.column("Y2",anchor=W,width=Wn)
table_pd.column("X3",anchor=W,width=Wn)
table_pd.column("Y3",anchor=W,width=Wn)
table_pd.column("Angle",anchor=W,width=Wn)
table_pd.column("Event",anchor=W,width=Wn)
table_pd.column("Frame",anchor=W,width=Wn)
#table_pd.column("Zoom",anchor=W,width=Wn)

table_pd.heading("#0",text="",anchor=W)
table_pd.heading("X1",text="X1",anchor=W)
table_pd.heading("Y1",text="Y1",anchor=W)
table_pd.heading("X2",text="X2",anchor=W)
table_pd.heading("Y2",text="Y2",anchor=W)
table_pd.heading("X3",text="X3",anchor=W)
table_pd.heading("Y3",text="Y3",anchor=W)
table_pd.heading("Angle",text="Angle",anchor=W)
table_pd.heading("Event",text="Event",anchor=W)
table_pd.heading("Frame",text="Frame",anchor=W)
#table_pd.heading("Zoom",text="Zoom",anchor=W)

table_pd.grid(column=3,row=0,sticky=NW,rowspan=3,columnspan=5)
#####Callbacks
def angle(points):
    #print(points)
    XA=points[1][0]-points[0][0]
    YA=points[1][1]-points[0][1]
    XB=points[1][0]-points[2][0]
    YB=points[1][1]-points[2][1]
    PDOT=XA*XB+YA*YB
    RA=np.sqrt(XA*XA+YA*YA)
    RB=np.sqrt(XB*XB+YB*YB)
    theta=np.arccos(PDOT/(RA*RB))
    #print(theta)
    return theta

def open_csv():
    global canvas,imtiff,image,xi,xf,yi,yf
    global ax,bx,ay,by
    root.filename = fd.askopenfilename(initialdir='.', title="Choose File",  filetypes=(("All files","*.*"),("tiff","*.tiff")))
    imtiff=io.imread(root.filename)
    imtiff = (imtiff >> 8).astype('uint8')
    im=imtiff[0,ax:bx,ay:by]
    image = Image.fromarray(im)
    image = image.resize((800,800),Image.ANTIALIAS)
    image = ImageTk.PhotoImage(image)
    canvas.create_image(400,400, anchor=CENTER, image=image)

def save_csv():
    global FN
    print(FN)
    with open(FN,"w",newline="") as save_file:
        csvwriter = csv.writer(save_file, delimiter=",")
        for row_id in table_pd.get_children():
            row = table_pd.item(row_id)['values']
            print('save_row:',row)
            csvwriter.writerow(row)

def load_csv():
    global FN
    root.filename = fd.askopenfilename(initialdir='.', title="Choose File",  filetypes=(("All files","*.*"),("csv","*.csv")))
    FN=root.filename
    with open(root.filename) as load_file:
        csvread = csv.reader(load_file, delimiter=',')
        for row in csvread:
            print('load row:', row)
            table_pd.insert("", 'end', values=row)
    plot_angles()



def framen(event):
    global canvas,imtiff,image,brth,xi,xf,yi,yf
    global ax,bx,ay,by,allyn
    m=int(event)
    #print(m)
    im=float(brth.get())*imtiff[m,ax:bx,ay:by]
    image = Image.fromarray(im)
    image = image.resize((800,800),Image.ANTIALIAS)
    image = ImageTk.PhotoImage(image)
    canvas.create_image(400, 400, anchor=CENTER, image=image)
    #print(allyn)
    if allyn.get()==True:
        plot_angles()
    

def setbr(event):
    global canvas,imtiff,image,frn,xi,xf,yi,yf
    global ax,bx,ay,by
    K=int(frn.get())
    #print(K,float(event))
    im=float(event)*imtiff[K,ax:bx,ay:by]
    image = Image.fromarray(im)
    image = image.resize((800,800),Image.ANTIALIAS)
    image = ImageTk.PhotoImage(image)
    canvas.create_image(400, 400, anchor=CENTER, image=image)
    plot_angles()

def get_xy_zoom(event):
    global sx,sy,countx
    if countx==0:
        sx,sy = event.x, event.y
    countx+=1

def zoom_sq(event):
    global sx, sy, countx,canvas,lj,xi,xf,yi,yf,ax,bx,ay,by
    global N1x,N1y,DLH

    canvas.create_oval(event.x-5,event.y-5,event.x+5,event.y+5,fill='',outline='yellow')
    if countx==2:
        N1x=sx 
        N1y=sy
        N2x= event.x
        DLH=N2x-N1x
        print("****")
        print("****")
        #TRANSTEST
        u1x=N1x
        u1y=N1y
        u2x=N2x
        u2y=N1y+DLH

        xi=u1x
        yi=u1y
        xf=u2x
        yf=u2y

        print(u1x,u2x,DLH)
        print(u1y,u2y,DLH)
        print("****")
        print("****")
        canvas.create_rectangle(N1x,N1y,N2x,N1y+DLH,outline='yellow')
        ay=int(np.floor(N1x/lj))
        by=int(np.floor((N1x+DLH)/lj))
        ax=int(np.floor(N1y/lj))
        bx=int(np.floor((N1y+DLH)/lj))

def plot_angles():
    global canvas,frn,xi,xf,yi,yf
    for row_id in table_pd.get_children():
            row = table_pd.item(row_id)['values']
            #canvas.create_oval(event.x-5,event.y-5,event.x+5,event.y+5,fill='',outline='blue')
            #print(row)
            pax,pay = Transfcoords(float(row[0]) , float(row[1]) )
            pbx,pby = Transfcoords(float(row[2]) , float(row[3]) )
            pcx,pcy = Transfcoords(float(row[4]) , float(row[5]) )

            canvas.create_oval(pax-5,pay-5,pax+5,pay+5,fill='',outline='blue')
            canvas.create_oval(pbx-5,pby-5,pbx+5,pby+5,fill='',outline='blue')
            canvas.create_oval(pcx-5,pcy-5,pcx+5,pcy+5,fill='',outline='blue')
            canvas.create_line((pax,pay,pbx,pby),fill='yellow',width=0.1)
            canvas.create_line((pbx,pby,pcx,pcy),fill='yellow',width=0.1)

    
def replotframe():
    global canvas,imtiff,image,frn,xi,xf,yi,yf
    global ax,bx,ay,by,allyn
    K1=int(frn.get())
    K2=float(brth.get())
    im=K2*imtiff[K1,ax:bx,ay:by]
    image = Image.fromarray(im)
    image = image.resize((800,800),Image.ANTIALIAS)
    image = ImageTk.PhotoImage(image)
    canvas.create_image(400, 400, anchor=CENTER, image=image)
    if allyn.get() == True:
        allyn.set(False)
        #show_sel()
    else:
        allyn.set(True)
        plot_angles()
    


def zoom_out():
    global xi,xf,yi,yf,countx
    global ax,bx,ay,by,DLH
    xi=0
    xf=800
    yi=0
    yf=800
    ax=0
    bx=512
    ay=0
    by=512
    DLH=LH
    countx=0
    replotframe()
    plot_angles()

def zoom_in():
    global xi,xf,yi,yf,countx
    replotframe()
    plot_angles()

def get_xy(event):
    global lx, ly, points
    lx,ly = event.x,event.y
    points.append((lx,ly))

def Abscoords(xn,yn):
    global xi,yi,DLH,LH,lh,lj
    #global g1x,g1y,g2x,g2y
    kj=1.0/lj
    qlj=DLH/LH
    px = kj*(xi+qlj*xn) 
    py=  kj*(yi+qlj*yn)
    print(px,py)
    return px,py

def Transfcoords(xn,yn):
    global xi,yi,DLH,LH,lh,lj
    kj=lj
    qlj=LH/DLH
    px = qlj*(xn*kj-xi)
    py = qlj*(yn*kj-yi)
    return px,py 

def click_count(event):
    global n, lx,ly,points,canvas,count
    n+=1
    #print(n)
    canvas.create_oval(event.x-5,event.y-5,event.x+5,event.y+5,fill='',outline='blue')
    if n%3==0:
        ptabs=[]
        ############Below is for tests-remove once it is properly checked
        pttrans=[]
        for pt in points:
            #print(pt)
            ux,uy = Abscoords(pt[0],pt[1])
            #print(ux,uy)
            ptabs.append((ux,uy))
        for pt in ptabs:
            ux,uy = Transfcoords(pt[0],pt[1])
            pttrans.append((ux,uy))
        print("********")
        print(points)
        print(ptabs)
        print(pttrans)
        print("********")
        ##################
        K=int(frn.get())
        canvas.create_line((points[0][0],points[0][1],points[1][0],points[1][1]),fill='yellow',width=0.1)
        canvas.create_line((points[1][0],points[1][1],points[2][0],points[2][1]),fill='yellow',width=0.1)
        phi=angle(points)
        #table_pd.insert(parent='',index='end',iid=count,values=(points[0][0],points[0][1],points[1][0],points[1][1],
        #                points[2][0],points[2][1],phi,'U',K))
        table_pd.insert(parent='',index='end',iid=count,values=(ptabs[0][0],ptabs[0][1],ptabs[1][0],ptabs[1][1],
                        ptabs[2][0],ptabs[2][1],phi,'U',K))
        count+=1
        n=0
        points=[]

def coll_option(event):  
    global count, table_pd
    sel = table_pd.focus()
    #print(sel,event)
    table_pd.set(sel, column="Event", value=event)

def del_row():
    global count, table_pd
    q=table_pd.selection()[0]
    table_pd.delete(q)
    replotframe()
    plot_angles()

def show_sel():
    global canvas,frn,xi,xf,yi,yf
    q=table_pd.selection()
    for p in q:
        #print(p.values)
        row = table_pd.item(p)['values']
        print(row)
        pax,pay = Transfcoords(float(row[0]) , float(row[1]) )
        pbx,pby = Transfcoords(float(row[2]) , float(row[3]) )
        pcx,pcy = Transfcoords(float(row[4]) , float(row[5]) )

        canvas.create_oval(pax-5,pay-5,pax+5,pay+5,fill='',outline='blue')
        canvas.create_oval(pbx-5,pby-5,pbx+5,pby+5,fill='',outline='blue')
        canvas.create_oval(pcx-5,pcy-5,pcx+5,pcy+5,fill='',outline='blue')
        canvas.create_line((pax,pay,pbx,pby),fill='yellow',width=0.1)
        canvas.create_line((pbx,pby,pcx,pcy),fill='yellow',width=0.1)



####################################

canvas.bind("<Button-1>",get_xy)
canvas.bind("<ButtonRelease-1>",click_count)


canvas.bind("<Button-3>",get_xy_zoom)
canvas.bind("<ButtonRelease-3>",zoom_sq)


####NEW-SAVE-LOAD-BUTTONS 
nw_btn=Button(btnframe,text='New/Load-TIFF',fg="red",activebackground = "red",command=open_csv)  
nw_btn.pack(side = LEFT)  
  
sv_btn = Button(btnframe, text="Save", fg="brown", activebackground = "brown",command=save_csv)  
sv_btn.pack(side = RIGHT)

ld_btn = Button(btnframe, text="Load-CSV", fg="white", activebackground = "white",command=load_csv)  
ld_btn.pack(side = RIGHT) 

#######################
### Sliders
frn = Scale(imframe,from_=0, to = 199, orient=HORIZONTAL,command=framen,variable=current_value)
frn.set(0)
frn.pack()
brth = Scale(cnvframe,from_=1, to=10,resolution=0.5,orient=VERTICAL,command=setbr,variable=brvalue)
brth.set(1.0)
brth.grid(column=2,row=0)
#########################
###TOOL BUTTONS

collmenu=OptionMenu(cnvframe,colvalue,*EList,command=coll_option)
collmenu.grid(column=3,row=1,sticky=W)

RecButt = Button(cnvframe, text='Reset-View',command=zoom_out)
RecButt.grid(column=4,row=1,sticky=W)
DelButt = Button(cnvframe, text='Zoom-In', command=zoom_in)
DelButt.grid(column=5,row=1,sticky=W)
HideAll = Button(cnvframe, text='Hide/Show-All', command=replotframe)
HideAll.grid(column=6,row=1,sticky=W)
DrawSel = Button(cnvframe, text='Remove-row',command=del_row)
DrawSel.grid(column=7,row=1,sticky=W)

#####
MoreA = Button(cnvframe, text='Show-selected',command=show_sel)
MoreA.grid(column=3,row=2,sticky=W)
MoreB = Button(cnvframe, text='More1')
MoreB.grid(column=4,row=2,sticky=W)
MoreC = Button(cnvframe, text='More2')
MoreC.grid(column=5,row=2,sticky=W)
MoreD = Button(cnvframe, text='More3')
MoreD.grid(column=6,row=2,sticky=W)
MoreE = Button(cnvframe, text='More4')
MoreE.grid(column=7,row=2,sticky=W)


#######################################
root.bind("<Left>", lambda e:frn.set(frn.get()-1) )
root.bind("<Right>", lambda e:frn.set(frn.get()+1) )

root.bind("<Up>", lambda e:brth.set(brth.get()-0.5) )
root.bind("<Down>", lambda e:brth.set(brth.get()+0.5) )

root.mainloop()
